# PHP Restful Backend API

## Description
A backend API class made with PHP which has the following methods:
1. httpGet() - performs a SELECT * on the database. Returns a row whose id match the payload parameter
2. httpPost() - performs an INSERT operation to the database. Returns the id and the inserted data in the payload parameter 
3. httpPut() - performs an UPDATE operation to an existing row in the database. Returns the updated data
4. httpDelete() - performs a DELETE operation on the database. Deletes rows whose ids match the payload. 

## Issues encountered
<li>Difficulty parsing/decoding the payload and request uri at the httpDelete() method. I was able to solve this issue by directly using the 'id' named key instead of the whole payload variable.
<li>Determining test dependencies and distributing the produced variable to its dependent consumers.  

## Test cases
1. Successful GET Request
2. Invalid GET Request
3. Successful POST Request
4. Invalid POST Request
5. Successful PUT Request
6. Invalid PUT Request
7. Successful DELETE Request
8. Invalid DELETE Request

<p> Failed requests due to failed sql queries were excluded</p>