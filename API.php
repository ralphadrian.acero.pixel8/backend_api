<!-- 
    Project Name: PHP RESTFUL Backend API
    Author: Ralph Adrian Jacob Acero
    Date: July 11, 2023
    Description: A backend API class made with PHP which has the following methods:
                1. httpGet() - performs a SELECT * on the database. Returns a row whose id match the payload parameter
                2. httpPost() - performs an INSERT operation to the database. Returns the id and the inserted data in the payload parameter 
                3. httpPut() - performs an UPDATE operation to an existing row in the database. Returns the updated data
                4. httpDelete() - performs a DELETE operation on the database. Deletes rows whose ids match the payload. (request uri should be [1234,5678])
 -->
<?php 
/**
   * Tells the browser to allow code from any origin to access
   */
//   header("Access-Control-Allow-Origin: *");


  /**
   * Tells browsers whether to expose the response to the frontend JavaScript code
   * when the request's credentials mode (Request.credentials) is include
   */
//   header("Access-Control-Allow-Credentials: true");
 


  /**
   * Specifies one or more methods allowed when accessing a resource in response to a preflight request
   */
//   header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
 
  /**
   * Used in response to a preflight request which includes the Access-Control-Request-Headers to
   * indicate which HTTP headers can be used during the actual request
   */
//   header("Access-Control-Allow-Headers: Content-Type");

  require_once('MysqliDb.php');

  class API {
    // initialize database attribute
    private $db;
    // constructor method
    public function __construct()
    {
        $this->db = new MysqliDb('localhost', 'root', '', 'employee');
    }

    // class methods
    /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload)
    {
        if(empty($payload) || !is_array($payload)) {                    // validate parameters    
            return json_encode(array(
                'method' => 'GET',
                'status' => 'failed',
                'message' => 'Invalid payload',
            ));
        } else {
            try {                                                     
                foreach($payload as $column => $value) {
                    $this->db->where($column, $value);
                }    
                $employees = $this->db->get('information');
                return json_encode(array(
                    'method' => 'GET',
                    'status' => "success",
                    'data' => $employees,
                ));                
            } catch (\Throwable $th) {
                return json_encode(array(
                    'method' => 'GET',
                    'status' => 'failed',
                    'message' => 'Failed Fetch Request',
                )); 
            }
        }
    }

    /**
    * HTTP POST Request
    *
    * @param $payload
    */
    public function httpPost($payload)
    {
        if(empty($payload) || !is_array($payload)) {                    // validate parameters
            return json_encode(array(
                'method' => 'POST',
                'status' => 'failed',
                'message' => 'Invalid payload',
            ));
        } else {
            $insert_data = $this->db->insert('information',$payload);
            if($insert_data) {
                try {                                           // retrieve inserted data for return response
                    $insertId = $this->db->getInsertId();
                    $this->db->where('id', $insertId);
                    $data = $this->db->get('information');
                    return json_encode(array(
                        'method' => 'POST',
                        'status' => "success",
                        'data' => $data,
                    ));
                } catch (\Throwable $th) {
                    //throw $th;
                    return json_encode(array(
                        'method' => 'POST',
                        'status' => 'failed',
                        'message' => 'Error retrieving inserted data',
                    ));
                }
            }else {
                return json_encode(array(
                    'method' => 'POST',
                    'status' => 'failed',
                    'message' => 'Failed to Insert data',
                )); 
            }
        }
    }

    /**
    * HTTP PUT Request
    *
    * @param $id
    * @param $payload
    */
    public function httpPut($id, $payload)
    {
        if (empty($id) || $id != $payload['id'] || empty($payload) || !is_array($payload)) {                    // validate parameters
            return json_encode(array(
                'method' => 'PUT',
                'status' => 'failed',
                'message' => 'Invalid payload',
            ));
        } else {
            $this->db->where('id', $id);
            $update_data = $this->db->update('information',$payload);
            if($update_data) {
                try {                                       // retrieve updated data for return response
                    $this->db->where('id', $id);
                    $data = $this->db->get('information');
                    return json_encode(array(
                        'method' => 'PUT',
                        'status' => "success",
                        'data' => $data,
                    ));
                } catch (\Throwable $th) {
                    return json_encode(array(
                        'method' => 'POST',
                        'status' => 'failed',
                        'message' => 'Error retrieving updated data',
                    ));
                }  
            }else {
                return json_encode(array(
                    'method' => 'PUT',
                    'status' => 'failed',
                    'message' => 'Failed to Update Data',
                )); 
            }
        }
    }
    /**
    * HTTP DELETE Request
    *
    * @param $id
    * @param $payload
    */
    public function httpDelete($id, $payload)
    {   
        // decode $id parameter to match payload (request uri should be [1234,5678])
        $id = json_decode(urldecode($id));

        if (empty($id) || empty($payload) || $id !== $payload['id']) {                    // validate parameters
            return json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'Invalid payload',
            ));
        }
        try {
            if (!is_array($payload['id'])) {        // check if payload is an array
                $this->db->where('id', $id);
            } else {
                $this->db->where('id', $payload['id'], 'IN');
            }
            
            $delete_data = $this->db->delete('information');    // perform delete operation
            
            if ($delete_data) {
                if($this->db->count == 0) {
                    $payload = array();             // set payload to empty array if no records were deleted
                }
                return json_encode(array(
                    'method' => 'DELETE',
                    'status' => 'success',
                    'data' => $payload,
                ));
            } else {
                return json_encode(array(
                    'method' => 'DELETE',
                    'status' => 'failed',
                    'message' => 'Failed to Delete Data',
                ));
            }
        } catch (\Throwable $th) {                  // catch error while deleting record
            return json_encode(array(
                'method' => 'DELETE',
                'status' => 'failed',
                'message' => 'Error deleting data',
            ));
        }
    }
  }

    //Identifier if what type of request
    // $request_method = $_SERVER['REQUEST_METHOD'];
    
    // // For GET,POST,PUT & DELETE Request
    // if ($request_method === 'GET') {
    //     $received_data = $_GET;
    // } else {
    // //check if method is PUT or DELETE, and get the ids on URL
    // if ($request_method === 'PUT' || $request_method === 'DELETE') {
    //     $request_uri = $_SERVER['REQUEST_URI'];


    //     $ids = null;
    //     $exploded_request_uri = array_values(explode("/", $request_uri));


    //     $last_index = count($exploded_request_uri) - 1;


    //     $ids = $exploded_request_uri[$last_index];


    //     }
    // }

    // //payload data
    // $received_data = json_decode(file_get_contents('php://input'), true);

    // $api = new API;


    // //Checking if what type of request and designating to specific functions
    // switch ($request_method) {
    //     case 'GET':
    //         $api->httpGet($received_data);
    //         break;
    //     case 'POST':
    //         $api->httpPost($received_data);
    //         break;
    //     case 'PUT':
    //         $api->httpPut($ids, $received_data);
    //         break;
    //     case 'DELETE':
    //         $api->httpDelete($ids, $received_data);
    //         break;
    // }
   

?>