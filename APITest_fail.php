<?php
    require_once 'API.php';
    use PHPUnit\Framework\TestCase;

    class APITest_fail extends TestCase
    {
        protected $api;

        protected function setUp(): void
        {
            $this->api = new API();
        }
        
        /**
         *  TWO scenarios for failed response  
         *  1. Invalid payload
         *  2. Failed sql query
        */ 

        public function testHttpPost()
        {
            $_SERVER['REQUEST_METHOD'] = 'POST';

            $payload = array(
                // 'first_name' => 'Test',
                // 'middle_name' => 'test',
                // 'last_name' => 'last test',
                // 'contact_number' => 654655
            );
            
            $result = json_decode($this->api->httpPost($payload), true);
            if (empty($payload) || !is_array($payload)) {
                $this->assertEquals('failed', $result['status']);
                $this->assertEquals('Invalid payload', $result['message']);
            } else {
                $this->assertEquals('failed', $result['status']);
                $this->assertEquals('Failed to Insert data', $result['message']);
            }

            $postId = NULL;

            return $postId;
        }

        /**
         * @depends testHttpPost
         */
        public function testHttpGet($postId)
        {
            $_SERVER['REQUEST_METHOD'] = 'GET';

            $payload = $postId;

            $result = json_decode($this->api->httpGet($payload), true);
            $this->assertEquals($result['method'], 'GET');

            if ($payload === null || !is_array($payload) || empty($payload)) {
                $this->assertEquals('failed', $result['status']);
                $this->assertEquals('Invalid payload', $result['message'], );
            } else {
                $this->assertEquals('failed', $result['status']);
                $this->assertEquals('Failed Fetch Request', $result['message']);
            }
        }

        /**
         * @depends testHttpPost
         */
        public function testHttpPut($postId)
        {
            $_SERVER['REQUEST_METHOD'] = 'PUT';

            $id = $postId;
            $payload = array(
                'id' => $id,
                'first_name' => 'Test',
                'middle_name' => 'test',
                'last_name' => 'last test',
                'contact_number' => 654655
            );

            $result = json_decode($this->api->httpPut($id, $payload), true);

            if (empty($id) || $payload === null || !is_array($payload) || empty($payload) || $id !== $payload['id']) {
                $this->assertEquals('failed', $result['status']);
                $this->assertEquals('Invalid payload', $result['message']);
            } else {
                $this->assertEquals('failed', $result['status']);
                $this->assertEquals('Failed to Update Data', $result['message']);
            }
        }

        /**
         * @depends testHttpPost
         */
        public function testHttpDelete($postId)
        {
            $_SERVER['REQUEST_METHOD'] = 'DELETE';

            $id = $postId;
            $payload = array(
                'id' => $id,
            );

            $result = json_decode($this->api->httpDelete($id, $payload), true);
            if (empty($postId) || $payload === null || !is_array($payload) || empty($payload) || $postId !== $payload['id']) {
                $this->assertEquals('failed', $result['status']);
                $this->assertEquals('Invalid payload', $result['message']);
            } else {
                $this->assertEquals('failed', $result['status']);
                $this->assertEquals('SQL query failed', $result['message']);
            }
        }
    }
?>