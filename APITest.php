<?php
    require_once 'API.php';
    use PHPUnit\Framework\TestCase;

    class APITest extends TestCase
    {
        protected $api;

        protected function setUp(): void
        {
            $this->api = new API();
        }

        public function testHttpPost()
        {
            $_SERVER['REQUEST_METHOD'] = 'POST';
            
            $payload = array(
                'first_name' => 'Test',
                'middle_name' => 'test',
                'last_name' => 'last test',
                'contact_number' => 654655
            );

            $result = json_decode($this->api->httpPost($payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals('success', $result['status']);
            $this->assertArrayHasKey('data', $result);

            $postId = $result['data'][0]['id'];
            return $postId;
        }
        
        /**
         * @depends testHttpPost
         */
        public function testHttpGet($postId)
        {   
            $_SERVER['REQUEST_METHOD'] = 'GET';

            $payload = array(
                'id' => $postId,
            );

            $result = json_decode($this->api->httpGet($payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals('success', $result['status']);
            $this->assertArrayHasKey('data', $result);
        }

        /**
         * @depends testHttpPost
         */
        public function testHttpPut($postId)
        {
            $_SERVER['REQUEST_METHOD'] = 'PUT';

            $payload = array(
                'id' => $postId,
                'first_name' => 'Test',
                'middle_name' => 'test',
                'last_name' => 'last test',
                'contact_number' => 654655
            );
            
            $result = json_decode($this->api->httpPut($postId, $payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals('success', $result['status']);
            $this->assertArrayHasKey('data', $result);
            
        }

        /**
         * @depends testHttpPost
         */
        public function testHttpDelete($postId)
        {
            $_SERVER['REQUEST_METHOD'] = 'DELETE';

            $payload = array(
                'id' => $postId,
            );

            $result = json_decode($this->api->httpDelete($postId, $payload), true);
            $this->assertArrayHasKey('status', $result);
            $this->assertEquals('success', $result['status']);
            $this->assertArrayHasKey('data', $result);
        }
    }
?>